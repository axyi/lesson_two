FROM node:latest

COPY ./flatris /app

WORKDIR /app

RUN yarn install

RUN yarn build

EXPOSE 3000

ENTRYPOINT yarn start
