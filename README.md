## Setup and running

```
git clone https://gitlab.com/axyi/lesson_two.git
cd ./lesson_two
kubectl apply -f kuber.yml
```

Go to http://<host>:3000 for playing to Flatris
host - is your kubernetes external cluster IP.


## Flatris
[![Flatris](flatris.png)]
